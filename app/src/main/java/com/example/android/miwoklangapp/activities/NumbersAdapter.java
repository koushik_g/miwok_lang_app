package com.example.android.miwoklangapp.activities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.android.miwoklangapp.R;
import com.example.android.miwoklangapp.models.Word;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class NumbersAdapter extends ArrayAdapter<Word> {
    private Context context;
    private int mColorResourceId;

    public NumbersAdapter(@NonNull Context context, @NonNull ArrayList<Word> objects, @NonNull int colorResourceId) {
        super(context, 0, objects);
        this.context = context;
        this.mColorResourceId = colorResourceId;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View numberView = convertView;
        if (numberView == null) {
            numberView = LayoutInflater.from(context).inflate(R.layout.activity_list_item, parent,false);
        }
        Word item = getItem(position);

        LinearLayout layout = numberView.findViewById(R.id.text_layout);
        TextView miwok = numberView.findViewById(R.id.miwok_text);
        TextView english = numberView.findViewById(R.id.default_text);
        ImageView image = numberView.findViewById(R.id.image);

        int color = ContextCompat.getColor(context, mColorResourceId); //converting resourceID into actual resource
        layout.setBackgroundColor(color);
        miwok.setText(item.getMiwokTranslation());
        english.setText(item.getDefaultTranslation());

        if (item.isImageResourceSet()) {
            image.setVisibility(View.VISIBLE);
            image.setImageResource(item.getmResourceId());
        } else {
            image.setVisibility(View.GONE);
        }

        /*String fileName = item.getDefaultTranslation();
        if (fileName != null) {
            Drawable drawable = getDrawable(fileName.toLowerCase());
            if (drawable != null){
                image.setImageDrawable(drawable);
            }
        }*/
        return numberView;
    }

    private Drawable getDrawable(String fileName) {
        if (fileName != null) {
            fileName = fileName.replace(" ", "_");
            fileName = "family_" + fileName;
        }
        Drawable drawable = null;
        if (null != fileName) {
            int resId = getResId(fileName, R.raw.class);
            if (resId > -1){
                //drawable = ContextCompat.getDrawable(getContext(), resId);
                InputStream istr = context.getResources().openRawResource(resId);
                drawable = Drawable.createFromStream(istr, fileName);
            }
        }
        return drawable;
    }

    /**
     * In this method we get the resource ID of the resource based on the
     * resource name.
     * @param fileName  takes in the processed text value of the text
     * @param c is the raw sub class of R class*/
    private int getResId(String fileName, Class<?> c){
        try {
            Field idField = c.getDeclaredField(fileName);
            return idField.getInt(idField);
        } catch (Exception e) {
            return -1;
        }
    }
}
