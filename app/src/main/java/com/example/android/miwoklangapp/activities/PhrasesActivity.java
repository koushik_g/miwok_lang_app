package com.example.android.miwoklangapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.miwoklangapp.R;
import com.example.android.miwoklangapp.models.Word;

import java.util.ArrayList;

public class PhrasesActivity extends AppCompatActivity {
    private MediaPlayer mediaPlayer;
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };
    private AudioManager audioManager;
    private AudioManager.OnAudioFocusChangeListener listener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT || focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                mediaPlayer.pause();
                mediaPlayer.seekTo(0);
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                mediaPlayer.start();
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                releaseMediaPlayer();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        final ArrayList<Word> wordsList = new ArrayList<>();
        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        wordsList.add(new Word("minto wuksus", "Where are you going?", R.raw.phrase_where_are_you_going));
        wordsList.add(new Word("tinnә oyaase'nә", "What is your name?", R.raw.phrase_what_is_your_name));
        wordsList.add(new Word("oyaaset...", "My name is...", R.raw.phrase_my_name_is));
        wordsList.add(new Word("michәksәs?", "How are you feeling?", R.raw.phrase_how_are_you_feeling));
        wordsList.add(new Word("kuchi achit", "I’m feeling good.", R.raw.phrase_im_feeling_good));
        wordsList.add(new Word("әәnәs'aa?", "Are you coming?", R.raw.phrase_are_you_coming));
        wordsList.add(new Word("hәә’ әәnәm", "Yes, I’m coming.", R.raw.phrase_yes_im_coming));
        wordsList.add(new Word("әәnәm", "I’m coming.", R.raw.phrase_im_coming));
        wordsList.add(new Word("yoowutis", "Let’s go.", R.raw.phrase_lets_go));
        wordsList.add(new Word("әnni'nem", "Come here.", R.raw.phrase_come_here));

        NumbersAdapter adapter = new NumbersAdapter(this, wordsList, R.color.category_phrases);
        ListView listView = findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                releaseMediaPlayer();
                Word item = wordsList.get(position);
                int result = audioManager.requestAudioFocus(listener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    mediaPlayer = MediaPlayer.create(PhrasesActivity.this, item.getmAudioResourceId());
                    mediaPlayer.start();
                    mediaPlayer.setOnCompletionListener(mCompletionListener);
                }

            }
        });
    }
    private void releaseMediaPlayer() {
        if (null != mediaPlayer) {
            mediaPlayer.release();
            mediaPlayer = null;
            audioManager.abandonAudioFocus(listener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }
}
