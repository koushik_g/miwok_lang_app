package com.example.android.miwoklangapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.miwoklangapp.R;
import com.example.android.miwoklangapp.models.Word;

import java.util.ArrayList;

public class FamilyActivity extends AppCompatActivity {
    private MediaPlayer mediaPlayer;
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };
    private AudioManager audioManager;
    private AudioManager.OnAudioFocusChangeListener mListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                    focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                mediaPlayer.pause();
                mediaPlayer.seekTo(0); // as words are small we are playing them from start once they are paused
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                releaseMediaPlayer();
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                mediaPlayer.start();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        final ArrayList<Word> wordsList = new ArrayList<>();
        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        wordsList.add(new Word("әpә", "father", R.drawable.family_father, R.raw.family_father));
        wordsList.add(new Word("әṭa", "mother", R.drawable.family_mother, R.raw.family_mother));
        wordsList.add(new Word("angsi", "son", R.drawable.family_son, R.raw.family_son));
        wordsList.add(new Word("tune", "daughter", R.drawable.family_daughter, R.raw.family_daughter));
        wordsList.add(new Word("taachi", "older brother", R.drawable.family_older_brother, R.raw.family_older_brother));
        wordsList.add(new Word("chalitti", "younger brother", R.drawable.family_younger_brother, R.raw.family_younger_brother));
        wordsList.add(new Word("teṭe", "older sister", R.drawable.family_older_sister, R.raw.family_older_sister));
        wordsList.add(new Word("kolliti", "younger sister", R.drawable.family_younger_sister, R.raw.family_younger_sister));
        wordsList.add(new Word("ama", "grandmother", R.drawable.family_grandmother, R.raw.family_grandmother));
        wordsList.add(new Word("paapa", "grandfather", R.drawable.family_grandfather, R.raw.family_grandfather));

        NumbersAdapter adapter = new NumbersAdapter(this, wordsList, R.color.category_family);
        ListView listView = findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                releaseMediaPlayer();
                Word item = wordsList.get(position);
                int result = audioManager.requestAudioFocus(mListener, AudioManager.STREAM_MUSIC,
                        AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    mediaPlayer = MediaPlayer.create(FamilyActivity.this, item.getmAudioResourceId());
                    mediaPlayer.start();
                    mediaPlayer.setOnCompletionListener(mCompletionListener);
                }

            }
        });
    }

    private void releaseMediaPlayer() {
        if (null != mediaPlayer) {
            mediaPlayer.release();
            mediaPlayer = null;
            audioManager.abandonAudioFocus(mListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }
}
