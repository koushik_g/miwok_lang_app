package com.example.android.miwoklangapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.miwoklangapp.R;
import com.example.android.miwoklangapp.models.Word;

import java.util.ArrayList;

public class NumbersActivity extends AppCompatActivity {
    private ArrayList<Word> wordsList = new ArrayList<>();
    private MediaPlayer mediaPlayer;
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };
    private AudioManager audioManager;
    private AudioManager.OnAudioFocusChangeListener listener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                mediaPlayer.start();
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                releaseMediaPlayer();
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK || focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                mediaPlayer.pause();
                mediaPlayer.seekTo(0);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        wordsList.add(new Word("Lutti", "One", R.drawable.number_one, R.raw.number_one));
        wordsList.add(new Word("Otiiko", "Two", R.drawable.number_two, R.raw.number_two));
        wordsList.add(new Word("Tolookosu", "Three", R.drawable.number_three, R.raw.number_three));
        wordsList.add(new Word("Oyyisa", "Four", R.drawable.number_four, R.raw.number_four));
        wordsList.add(new Word("Massokka", "Five", R.drawable.number_five, R.raw.number_five));
        wordsList.add(new Word("Temmokka", "Six", R.drawable.number_six, R.raw.number_six));
        wordsList.add(new Word("Kenekaku", "Seven", R.drawable.number_seven, R.raw.number_seven));
        wordsList.add(new Word("Kawinta", "Eight", R.drawable.number_eight, R.raw.number_eight));
        wordsList.add(new Word("Wo'e", "Nine", R.drawable.number_nine, R.raw.number_nine));
        wordsList.add(new Word("Na'aacha", "Ten", R.drawable.number_ten, R.raw.number_ten));

        /**
         * Creating ArrayAdapter object and Assigning it to the ListView
         * */
        NumbersAdapter numbersAdapter = new NumbersAdapter(this, wordsList, R.color.category_numbers);
        ListView numbersList = findViewById(R.id.list);
        numbersList.setAdapter(numbersAdapter);

        numbersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                releaseMediaPlayer();
                Word item = wordsList.get(position);
                int result = audioManager.requestAudioFocus(listener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    mediaPlayer = MediaPlayer.create(NumbersActivity.this, item.getmAudioResourceId());
                    mediaPlayer.start();
                    mediaPlayer.setOnCompletionListener(mCompletionListener);
                }
            }
        });
    }

    private void releaseMediaPlayer() {
        if (null != mediaPlayer) {
            mediaPlayer.release();
            mediaPlayer = null;
            audioManager.abandonAudioFocus(listener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }
}
