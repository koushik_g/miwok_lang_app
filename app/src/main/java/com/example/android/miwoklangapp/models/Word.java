package com.example.android.miwoklangapp.models;

public class Word {
    private String miwokTranslation;
    private String defaultTranslation;
    private int mResourceId;
    private int mAudioResourceId;
    private boolean isImageResourceSet;
    /*private int mResourceId = NO_IMAGE_RESOURCE;
    private static final int NO_IMAGE_RESOURCE = -1;*/
    public Word(String miwokTranslation, String defaultTranslation, int mAudioResourceId) {
        this.miwokTranslation = miwokTranslation;
        this.defaultTranslation = defaultTranslation;
        this.isImageResourceSet = false;
        this.mAudioResourceId = mAudioResourceId;
    }
    public Word(String miwokTranslation, String defaultTranslation, int mResourceId, int mAudioResourceId) {
        this.miwokTranslation = miwokTranslation;
        this.defaultTranslation = defaultTranslation;
        this.mResourceId = mResourceId;
        this.isImageResourceSet = true;
        this.mAudioResourceId = mAudioResourceId;
    }
    public String getMiwokTranslation() {
        return miwokTranslation;
    }
    public String getDefaultTranslation() {
        return defaultTranslation;
    }
    public int getmResourceId() {
        return mResourceId;
    }
    public boolean isImageResourceSet() {
        return isImageResourceSet;
    }
    public int getmAudioResourceId() {
        return mAudioResourceId;
    }
    /*public boolean hasImage() {
        return mResourceId != NO_IMAGE_RESOURCE;
    }*/
}
